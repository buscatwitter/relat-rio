# Relatório Técnico

Este repositório contém os dados técnicos de como a aplicação está perante aos requísitos propostos (Desenvolvimento, teste, deploy, CI/CD, etc ...)

1. [Tecnologias](#tecnologias)
2. [Diagrama](#diagrama)
3. [Frameworks](#frameworks)
4. [Ide's](#ide's)
5. [Endereços das Publicações](#endereços)
6. [Links](#links)
7. [Documentacao API](#backend-documentacão-swagger)
8. [Chamadas API](#backend-chamada-de-api-envio-de-requisição)

## Tecnologias
A aplicação de backend foi desenvolvimento utilizando basicamente a linguagem de programação `Java`, juntamento com seus frameworks, armazenando dados em um banco `NoSQL`, no caso `MongoDB`. Já o Frontend foi desenvolvido utilizando `JavaScript` com o framework `VueJS`.

Todo desenvolvimento está armazenado em repositórios do GitLab, ao push dos commits a `pipeline` para a entrega continua é iniciada, gerando ao final os artefatos entregues no repositório do `DockerHub`, em formato de uma imagem Docker.

O pipeline esta baseado no [Gitlab-CI](https://about.gitlab.com/).

Para os testes de backend foi utilizado o framework JUnit.

E para a publicação final do ecosistema, foi utilizado a Cloud pública do Google, com o serviço `Kubernetes Engine`.

## Diagrama
A estratégia para a construção da aplicação foi, segregar o backend utilizando uma linguagem de mercado com o framework voltado para microserviços (Java com SpringBoot), e o Frontend utilizando o conceito de Single Page Application, com o framework VueJS em JavaScript.
Os dados são persistidos em um banco de dados NoSQL, MongoDB e as consultas disparadas para aplicaçao utilizam o conceito de `cache`, aplicado com framework spring cache. 
Para o deploy da aplicação foi escolhido o fornecedor `Google` Cloud, utilizando o serviço `Kubernetes Engine`.

- Abaixo o diagrama detalhado

![diagram](img/diagrama.png)

## Frameworks

Para o Backend foi usado:
- Spring Boot
- Spring Data
    - Aggregation framework
- Spring Cache
- Swagger (Documentação)
- Actuator (Health check)

Para o Frontend foi usado:
- VueJS

## IDE's e Ferramentas
- Spring Tools
- VS Code
- Postman
- gcloud cli
- vue cli
- kubectl

## Endereços

Dockhub User: hrepulho

Endereço: [https://hub.docker.com/u/hrepulho](https://hub.docker.com/u/hrepulho)

Imagens das Aplicações

- [hrepulho/backend_buscatwitter](https://hub.docker.com/r/hrepulho/backend_buscatwitter)


- [hrepulho/frontend_buscatwitter](https://hub.docker.com/r/hrepulho/frontend_buscatwitter)

## Links

## Frontend
[http://34.95.212.92:8080/](http://34.95.212.92:8080)

- Home da Aplicação, exibe os Top 5 usuários das hashtags pesquisadas

![home](img/home.png)

- Pesquisa, fornece o envio de pesquisa por hashtag

![pesquisa](img/pesquisa.png)

![resposta](img/resposta.png)

## Backend - Documentacão (Swagger)

- [http://35.198.1.12:3000/swagger-ui.html#/](http://35.198.1.12:3000/swagger-ui.html#/)

## Backend - HealthCheck

- [http://35.198.1.12:3000/actuator](http://35.198.1.12:3000/actuator)

``` bash
# Chamada via Curl
curl --location --request GET 'http://35.198.1.12:3000/actuator/health'
```

``` bash
# Exemplo de Resposta
{
    "status": "UP",
    "components": {
        "diskSpace": {
            "status": "UP",
            "details": {
                "total": 101241290752,
                "free": 97430560768,
                "threshold": 10485760
            }
        },
        "mongo": {
            "status": "UP",
            "details": {
                "version": "4.2.3"
            }
        },
        "ping": {
            "status": "UP"
        }
    }
}
```

## Backend - Chamada de API - Envio de Requisição

- [http://35.198.1.12:3000/twitter/v1/hashtag](http://35.198.1.12:3000/twitter/v1/hashtag)

``` bash
# Chamada via Curl
curl --location --request POST 'http://35.198.1.12:3000/twitter/v1/hashtag' \
--header 'Content-Type: application/json' \
--data-raw '{
	"hashtag" : "#AWS"
}'
```

``` bash
# Exemplo de Resposta
{
    "hashtag": "#AWS",
    "qtd_tweets": 100
}
```

## Backend - Chamada de API - Consulta de Informação

[http://35.198.1.12:3000/twitter/v1/seguidores](http://35.198.1.12:3000/twitter/v1/seguidores)

``` bash
# Chamada via Curl
curl --location --request GET 'http://35.198.1.12:3000/twitter/v1/seguidores'
```

``` bash
# Exemplo de Resposta
[
    {
        "hashtag": "#AWS",
        "user_name": "【最新まとめ】まとめまとめ",
        "user_followers_count": 33562,
        "user_profile_image_url": "https://pbs.twimg.com/profile_images/643616991928971268/ROEGYIo0_normal.png"
    },
    {
        "hashtag": "#AWS",
        "user_name": "Avrohom Gottheil 🇺🇸 #MWC2020",
        "user_followers_count": 17891,
        "user_profile_image_url": "https://pbs.twimg.com/profile_images/1129430659066408963/zA_EYkyv_normal.png"
    },
    {
        "hashtag": "#AWS",
        "user_name": "ReleaseTEAM",
        "user_followers_count": 14694,
        "user_profile_image_url": "https://pbs.twimg.com/profile_images/1193982960397148160/GL06FHJn_normal.jpg"
    },
    {
        "hashtag": "#AWS",
        "user_name": "Robert Hof",
        "user_followers_count": 11663,
        "user_profile_image_url": "https://pbs.twimg.com/profile_images/28838992/hofdark_normal.jpg"
    },
    {
        "hashtag": "#AWS",
        "user_name": "Yan Cui",
        "user_followers_count": 8662,
        "user_profile_image_url": "https://pbs.twimg.com/profile_images/1106988904127713280/59jDYupv_normal.png"
    }
]
```